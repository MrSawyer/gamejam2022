#include "sprite.h"
#include "../../examples/sfml-renderer/sfml-renderer.h"
#include "../glm/glm/gtc/constants.hpp"

namespace gameapp
{
	CircleSprite::CircleSprite()
	{
		m_shape = std::make_shared<sf::CircleShape>();
		SetUpdateCallback(std::bind(&CircleSprite::UpdateCallback, this));
	}

	void CircleSprite::UpdateCallback()
	{
		m_shape->setPosition(GetPositionX(), GetPositionY());
		m_shape->setRadius(GetScaleX());
		m_shape->setOrigin(m_shape->getRadius(), m_shape->getRadius());
	}

	void CircleSprite::Draw(hospite::IRenderFrame* frame)
	{
		if (IsScheduledToUpdate())UpdateCallback();

		dynamic_cast<SFMLFrame*>(frame)->AddObject(m_shape);
	}

	void CircleSprite::SetColor(float r, float g, float b, float a)
	{
		m_shape->setFillColor(sf::Color(r, g, b, a));
	}

	float CircleSprite::GetAlpha() const
	{
		return m_shape->getFillColor().a;
	}

	void CircleSprite::SetAlpha(float alpha)
	{
		sf::Color color = m_shape->getFillColor();
		color.a = alpha;
		m_shape->setFillColor(color);
	}

	void LineSprite::SetEndpoint(float xEnd, float yEnd)
	{
		m_xEnd = xEnd;
		m_yEnd = yEnd;
		UpdateCallback();
	}

	void LineSprite::SetColor(float r, float g, float b, float a)
	{
		m_shape->setFillColor(sf::Color(r, g, b, a));
	}

	float LineSprite::GetAlpha() const
	{
		return m_shape->getFillColor().a;
	}

	void LineSprite::SetAlpha(float alpha)
	{
		sf::Color color = m_shape->getFillColor();
		color.a = alpha;
		m_shape->setFillColor(color);
	}

	RectangleSprite::RectangleSprite()
	{
		m_shape = std::make_shared<sf::RectangleShape>();
		SetUpdateCallback(std::bind(&RectangleSprite::UpdateCallback, this));

	}

	void RectangleSprite::UpdateCallback()
	{
		m_shape->setPosition(GetPositionX(), GetPositionY());
		m_shape->setSize(sf::Vector2f(GetScaleX(), GetScaleY()));

		m_shape->setOrigin(m_shape->getSize().x / 2.0f, m_shape->getSize().y / 2.0f);
	}

	void RectangleSprite::Draw(hospite::IRenderFrame* frame)
	{
		if (IsScheduledToUpdate())UpdateCallback();

		dynamic_cast<SFMLFrame*>(frame)->AddObject(m_shape);
	}

	void RectangleSprite::SetColor(float r, float g, float b, float a)
	{
		m_shape->setFillColor(sf::Color(r, g, b, a));
	}

	float RectangleSprite::GetAlpha() const
	{
		return m_shape->getFillColor().a;
	}

	void RectangleSprite::SetAlpha(float alpha)
	{
		sf::Color color = m_shape->getFillColor();
		color.a = alpha;
		m_shape->setFillColor(color);
	}

	LineSprite::LineSprite()
	{
		m_shape = std::make_shared<sf::RectangleShape>();
		SetUpdateCallback(std::bind(&LineSprite::UpdateCallback, this));
	}

	void LineSprite::UpdateCallback()
	{
		m_shape->setPosition(GetPositionX(), GetPositionY());
		m_shape->setSize(sf::Vector2f(glm::distance(GetPosition(), glm::vec3(m_xEnd, m_yEnd, 0)), GetScaleX()));
		m_shape->setOrigin(0, m_shape->getSize().y / 2.0f);

		float dx = GetPositionX() - m_xEnd;
		float dy = GetPositionY() - m_yEnd;

		m_alpha = std::atan2(dy, dx) * 180.0f / glm::pi<float>();
		m_shape->setRotation(m_alpha + 180.0f);
	}

	void LineSprite::Draw(hospite::IRenderFrame* frame)
	{
		if (IsScheduledToUpdate())UpdateCallback();

		dynamic_cast<SFMLFrame*>(frame)->AddObject(m_shape);
	}


}