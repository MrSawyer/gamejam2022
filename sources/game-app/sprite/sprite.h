#pragma once
#include <SFML/Graphics.hpp>
#include "../../framework/renderer/render-frame.h"
#include "../../engine/actor/actor.h"

namespace gameapp
{
	class ISprite : public virtual hospite::IActor
	{
	public:
		virtual ~ISprite() {}
		virtual void Draw(hospite::IRenderFrame* frame) = 0;
		virtual void SetColor(float r, float g, float b, float a = 255.0f) = 0;
		virtual float GetAlpha() const = 0;
		virtual void SetAlpha(float alpha) = 0;

		virtual void UpdateCallback() {};

		bool OnInit() override { return true; };
		void OnTick(const float timeDelta) override {};
		void OnDestroy() override {};
	};

	class CircleSprite : public virtual ISprite
	{
	public:
		CircleSprite();
		virtual ~CircleSprite() {}
		void Draw(hospite::IRenderFrame* frame) override;
		void SetColor(float r, float g, float b, float a = 255.0f) override;
		float GetAlpha() const override;
		void SetAlpha(float alpha) override;

		void UpdateCallback()override;

	private:
		std::shared_ptr<sf::CircleShape> m_shape;
	};

	class RectangleSprite : public virtual ISprite
	{
	public:
		RectangleSprite();
		virtual ~RectangleSprite() {}
		void Draw(hospite::IRenderFrame* frame) override;
		void SetColor(float r, float g, float b, float a = 255.0f) override;
		float GetAlpha() const override;
		void SetAlpha(float alpha) override;

		void UpdateCallback()override;
	private:
		std::shared_ptr < sf::RectangleShape> m_shape;
	};

	class LineSprite : public virtual ISprite
	{
	public:
		LineSprite();
		virtual ~LineSprite() {}
		void Draw(hospite::IRenderFrame* frame) override;
		void SetEndpoint(float xEnd, float yEnd);
		void SetColor(float r, float g, float b, float a = 255.0f) override;
		float GetAlpha() const override;
		void SetAlpha(float alpha) override;

		void UpdateCallback()override;
	private:
		std::shared_ptr < sf::RectangleShape> m_shape;
		float m_xEnd, m_yEnd;
		float m_alpha;
	};
}