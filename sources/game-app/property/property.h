#pragma once

namespace gameapp
{
	class Player;

	class IProperty
	{
	public:
		Player* GetOwner() const;
		void SetOwner(Player* player);

	protected:

	private:
		Player* m_owner;
	};
}