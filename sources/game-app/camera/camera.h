#pragma once
#include "../glm/glm/glm.hpp"

namespace gameapp
{
	class Camera
	{
	public:
		Camera();

		void Update();

		void ZoomIn();
		void ZoomOut();

		void SetSpeed(float speed);
		void SetAccelerationSpeed(float accelerationSpeed);
		void SetInertia(float inertia);
		void SetPosition(glm::vec2 position);

		void SetZoom(float zoom);
		void SetMaxZoomSpeed(float speed);
		void SetZoomDelta(float delta);
		void SetMaxZoom(float maxZoom);

		void SetBoundaries(glm::vec2 topLeft, glm::vec2 bottomRight);

		glm::vec2 GetDisplacement() const;
		glm::vec2 GetPosition() const;
		float GetZoom() const;

	private:
		float m_maxSpeed;
		float m_inertia;
		float m_accelerationSpeed;
		glm::vec2 m_displacement;
		glm::vec2 m_position;
		glm::vec2 m_boundaryTopLeft;
		glm::vec2 m_boundaryBottomRight;

		float m_maxZoomSpeed;
		float m_zoomDelta;
		float m_zoomAcceleration;
		float m_maxZoom;
		float m_zoom;
	};


}