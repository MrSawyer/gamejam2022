#pragma once
#include "../sprite/sprite.h"
#include "../structure/structure.h"
#include "../property/property.h"

#include <set>

namespace gameapp
{
	class IUnit;

	class Player;

	class Planet : virtual public CircleSprite, public IProperty
	{
	public:
		virtual ~Planet() {};
		void ConnectTo(Planet*);
		const std::set<Planet*>& GetConnected() const;
		void ClearConnections();
		bool IsConnected(Planet*) const;

		void RequestRemoval(IUnit * unit);
		void RequestAddition(IUnit* unit);

		void Update();

		std::map<Player*, std::set<IUnit*>> GetUnits() const;

	private:
		
		void AddUnit(IUnit* unit);
		void RemoveUnit(IUnit* unit);

		void PerformPhase();

		void UpdateUnitsExistance();

		std::set<Planet*> m_connected;
		std::map<Player *, std::set<IUnit*>> m_units;

		std::list<IUnit*> m_toAdd;
		std::list<IUnit*> m_toRemove;

	};
}