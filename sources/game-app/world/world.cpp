#include <LuaContext.hpp>
#include <fstream>

#include "world.h"
#include "../glm/glm/gtc/constants.hpp"
#include <set>
#include <stack>
#include <map>

namespace gameapp
{

	bool World::LoadWorldSpecification(const std::string& fileName)
	{
		LuaContext lua;

		std::ifstream script("../sources/scripts/"+ fileName);
		lua.executeCode(script);
		script.close();

		auto lua_worldName = lua.readVariable<std::string>("WorldName");
		//if(lua_worldName.empty())

		return true;
	}

	bool World::CheckConnectivity()const
	{
		std::size_t DFSCount = 0;
		std::set<Planet*> visited;
		std::stack<Planet*> toCheck;

		toCheck.push(m_planets[0].get());
		visited.insert(m_planets[0].get());

		while (!toCheck.empty())
		{
			Planet* v = toCheck.top();
			toCheck.pop();
			DFSCount++;

			for (const auto & nei : v->GetConnected())
			{
				if (visited.contains(nei))continue;

				visited.insert(nei);
				toCheck.push(nei);
			}
		}

		return DFSCount == m_planets.size();
	}

	void World::Destroy()
	{
		m_planets.clear();
	}

	void World::Generate(WorldSpecification specification)
	{
		m_specification = std::move(specification);
		m_planets.reserve(2 * m_specification.m_planetCount);
		
		m_worldSize = sqrt(m_specification.m_planetSize * m_specification.m_planetCount);
		
		GeneratePlanets();
		do {
			if (!RearangePlanets())
			{
				m_worldSize *= 1.07;
				continue;
			}

			ConnectPlanets();

			if (!CheckConnectivity())
			{
				m_worldSize /= 1.06;
				continue;
			}
			break;
		} while (true);
	}

	WorldSpecification World::GetWorldInfo()const
	{
		return m_specification;
	}

	float World::GetWorldSize()const
	{
		return m_worldSize;
	}

	void World::ConnectPlanets()
	{
		for (auto & planet : m_planets)
		{
			planet->ClearConnections();
		}

		for (std::size_t i = 0; i < m_planets.size(); ++i)
		{
			// distance -> iterator
			std::map<float, std::size_t> neighbours;

			for (std::size_t ii = 0; ii < m_planets.size(); ++ii)
			{
				if (i == ii)continue;

				float distance = glm::distance(m_planets[i]->GetPosition(), m_planets[ii]->GetPosition());
				if (distance < m_specification.m_connectionDistance)
				{
					neighbours.insert(std::make_pair(distance,ii));
				}
			}

			std::size_t madeConnections = 0;
			for (auto it = neighbours.begin(); it != neighbours.end(); ++it)
			{
				if (madeConnections >= m_specification.m_maxConnections)break;
				madeConnections++;

				m_planets[i]->ConnectTo(m_planets[it->second].get());
				m_planets[it->second]->ConnectTo(m_planets[i].get());
			}


		}
	}

	void World::GeneratePlanets()
	{
		for (std::size_t i = 0; i < 2 * m_specification.m_planetCount; ++i)
		{
			m_planets.push_back(std::unique_ptr<Planet>(new Planet));
			m_planets.back()->SetScale(glm::vec3((float)m_specification.m_planetSize));
		}
	}


	bool World::RearangePlanets()
	{
		const std::size_t MAX_ITERATIONS = 100;
		std::set<Planet*> m_alreadyGeneratedPlanets;

		for (std::size_t i = 0; i < m_specification.m_planetCount; ++i)
		{
			glm::vec3 position;
			std::size_t iteration = 0;

			for(iteration = 0; iteration < MAX_ITERATIONS; ++iteration)
			{
				/*
				* https://stackoverflow.com/questions/5837572/generate-a-random-point-within-a-circle-uniformly
				*/
				float r = m_worldSize * glm::pow((float)std::rand() / (float)RAND_MAX, 0.25);
				float theta = ((float)std::rand() / (float)RAND_MAX) * 2.0f * glm::pi<float>();


				//position.x = (float)(std::rand() % (int)m_worldSize) - (m_worldSize / 2.0f);
				//position.y = (float)(std::rand() % (int)m_worldSize) - (m_worldSize / 2.0f);
				position.x = r * glm::cos(theta);
				position.y = r * glm::sin(theta);

				position.z = 0;

				m_planets[i]->SetPosition(position);
				m_planets[i + m_specification.m_planetCount]->SetPosition(-position);

				if (glm::length(position) < m_specification.m_minDistance/2.0f)continue;

				if (CheckDistance(i, m_alreadyGeneratedPlanets) && CheckDistance(i + m_specification.m_planetCount, m_alreadyGeneratedPlanets))
				{
					m_alreadyGeneratedPlanets.insert(m_planets[i].get());
					m_alreadyGeneratedPlanets.insert(m_planets[i + m_specification.m_planetCount].get());

					break;
				}
			} 

			if (iteration == MAX_ITERATIONS)
			{
				return false;
			}
		}

		return true;
	}

	bool World::CheckDistance(const std::size_t planetId, const std::set<Planet*> & m_alreadyGeneratedPlanets)const
	{
		for (const auto & planet : m_alreadyGeneratedPlanets)
		{
			float distance = glm::distance(planet->GetPosition(), m_planets[planetId]->GetPosition());
			if (distance < m_specification.m_minDistance)
				return false;
		}
		return true;
	}

	void World::FindSpawnPoints()
	{
		float maxDistance = 0;
		std::size_t spawnPlanet = 0;

		for (std::size_t i = 0; i < m_specification.m_planetCount; ++i)
		{
			float distance = glm::distance(m_planets[i]->GetPosition(), m_planets[i + m_specification.m_planetCount]->GetPosition());
			if (distance > maxDistance)
			{
				spawnPlanet = i;
				maxDistance = distance;
			}
		}

		m_spawnPlanet = spawnPlanet;
	}

	void World::Render(hospite::IRenderFrame* frame)
	{
		for (std::size_t i = 0; i < m_planets.size(); ++i)
		{
			for (const auto & otherPlanet : m_planets[i]->GetConnected())
			{
				LineSprite line;
				line.SetScaleX(20);
				line.SetColor(255, 255, 255, 128);
				line.SetPosition(m_planets[i]->GetPosition());
				line.SetEndpoint(otherPlanet->GetPositionX(), otherPlanet->GetPositionY());
				
				line.Draw(frame);
			}
		}

		LineSprite line;
		line.SetScaleX(5);
		line.SetPosition(-100, 0, 0);
		line.SetEndpoint(100, 0);
		line.Draw(frame);

		LineSprite line2;
		line2.SetScaleX(5);
		line2.SetPosition(0, -100, 0);
		line2.SetEndpoint(0, 100);
		line2.Draw(frame);

		for (std::size_t i = 0; i < m_planets.size(); ++i)
		{
			if (i < m_specification.m_planetCount)m_planets[i]->SetColor(0, 255, 0);
			m_planets[i]->Draw(frame);
		}
	}

}