#include "planets.h"
#include "../ships/ship.h"
#include "../../framework/logger/logger.h"

namespace gameapp
{
	void Planet::ConnectTo(Planet* planet)
	{
		m_connected.insert(planet);
	}

	const std::set<Planet*>& Planet::GetConnected() const
	{
		return m_connected;
	}

	void Planet::ClearConnections()
	{
		m_connected.clear();
	}

	bool Planet::IsConnected(Planet* planet) const
	{
		return m_connected.contains(planet);
	}

	void Planet::AddUnit(IUnit* unit)
	{
		if (unit == nullptr)return;
		
		m_units[unit->GetOwner()].insert(unit);
	}
	void Planet::RemoveUnit(IUnit* unit)
	{
		if (unit == nullptr)return;
		if (m_units[unit->GetOwner()].find(unit) == m_units[unit->GetOwner()].end())return;

		m_units[unit->GetOwner()].erase(unit);
	}

	void Planet::UpdateUnitsExistance()
	{
		for (const auto& playerUnits : m_units)
		{
			for (const auto& unit : playerUnits.second)
			{
				ASSERT(unit->GetOccupiedPlanet() == this);
				unit->Update();
			}
		}

		for (const auto& unit : m_toRemove)RemoveUnit(unit);
		for (const auto& unit : m_toAdd)AddUnit(unit);

		m_toRemove.clear();
		m_toAdd.clear();
	}

	void Planet::Update()
	{
		PerformPhase();
	}

	std::map<Player*, std::set<IUnit*>> Planet::GetUnits() const
	{
		return m_units;
	}

	void Planet::RequestRemoval(IUnit* unit)
	{
		m_toRemove.push_back(unit);
	}

	void Planet::RequestAddition(IUnit* unit)
	{
		m_toAdd.push_back(unit);
	}

	void Planet::PerformPhase()
	{
		UpdateUnitsExistance();
		//-------
		for (const auto& playerUnits : m_units)
		{
			for (const auto& unit : playerUnits.second)
			{
				unit->PerformPrePhase();
			}
		}
		UpdateUnitsExistance();
		//------
		for (const auto& playerUnits : m_units)
		{
			for (const auto& unit : playerUnits.second)
			{
				unit->PerformPhase();
			}
		}
		UpdateUnitsExistance();
		//-------
		for (const auto& playerUnits : m_units)
		{
			for (const auto& unit : playerUnits.second)
			{
				unit->PerformPostPhase();
			}
		}
	}
}