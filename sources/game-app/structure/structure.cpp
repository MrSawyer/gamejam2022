#include "structure.h"
#include "../../framework/logger/logger.h"

namespace gameapp
{
	Structure::Structure(Planet* position, Player* owner, StructureBlueprint* blueprint)
		: IUnit(position, owner, blueprint)
	{
		m_blueprint = blueprint;
	}

	void Structure::PerformPrePhase()
	{
	}
	void Structure::PerformPhase()
	{
	}
	void Structure::PerformPostPhase()
	{
	}
}