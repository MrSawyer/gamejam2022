#pragma once
#include <unordered_map>
#include <type_traits>

#include "../resources/resources.h"
#include "../unit/unit.h"
#include "../ships/ship.h"
#include "../structure/structure.h"

namespace gameapp
{
	class Player
	{
	public:
		virtual ~Player() = default;

		void Update();

		void RequestRemoval(IUnit* unit);
		void RequestAdd(ShipBlueprint* blueprint, Planet* position);
		void RequestAdd(StructureBlueprint* blueprint, Planet* position);


	private:
		void AddUnit(ShipBlueprint* blueprint, Planet* position);
		void AddUnit(StructureBlueprint* blueprint, Planet* position);
		void RemoveUnit(IUnit* unit);

		std::unordered_map<ResourceType, float> resources;

		std::set<std::unique_ptr<IUnit>> m_units;


		std::list<std::pair<ShipBlueprint*, Planet*>> m_toAddShips;
		std::list<std::pair<StructureBlueprint*, Planet*>> m_toAddStructures;

		std::list<IUnit*> m_toRemove;
	};

}