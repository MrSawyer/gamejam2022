#include "unit.h"
#include "../world/planets.h"
#include "../player/player.h"
#include "../../framework/logger/logger.h"

namespace gameapp
{
	IUnit::IUnit(Planet* position, Player* owner, UnitBlueprint* blueprint)
	{
		SetOwner(owner);

		m_occupiedPlanet = position;
		m_occupiedPlanet->RequestAddition(this);

		m_buildPhase = 0;
		m_built = false;

		m_unitBlueprint = blueprint;

		m_hp = m_unitBlueprint->m_initialHp;
		m_resists = m_unitBlueprint->m_initialResists;
	}

	void IUnit::Destroy()
	{
		ASSERT_MSG(m_occupiedPlanet != nullptr, "Unit on non existing planet?!");

		if (m_occupiedPlanet)m_occupiedPlanet->RequestRemoval(this);
		if (GetOwner())GetOwner()->RequestRemoval(this);

		ISprite::RequestDestroy();
	}


	void IUnit::AbsorbDamage(const DamageType type, const float damage)
	{
		if (m_resists.find(type) == m_resists.end())
		{
			// NO RESIST for this dmg
			m_hp -= damage;
		}
		else
		{
			m_resists[type] -= damage;

			// resists broken
			if (m_resists[type] <= 0.0f)
			{
				m_hp -= std::abs(m_resists[type]);
				m_resists[type] = 0.0f;
			}
		}
	}

	void IUnit::SetOccupiedPlanet(Planet* planet)
	{
		ASSERT_MSG(m_occupiedPlanet != nullptr, "Unit on non existing planet?!");
		if (m_occupiedPlanet == nullptr)return;
		if (m_occupiedPlanet == planet)return;

		m_occupiedPlanet->RequestRemoval(this);
		m_occupiedPlanet = planet;
		m_occupiedPlanet->RequestAddition(this);	
	}

	Planet* IUnit::GetOccupiedPlanet() const
	{
		return m_occupiedPlanet;
	}

	float IUnit::GetHP() const
	{
		return m_hp;
	}

	bool IUnit::IsReady() const
	{
		return m_built;
	}

	const std::string& IUnit::GetTypeName() const
	{
		ASSERT_MSG(m_unitBlueprint != nullptr, "Unit without blueprint?!");

		if (!m_unitBlueprint)return "";
		return m_unitBlueprint->m_typeName;
	}

	void IUnit::Update()
	{
		if (m_hp <= 0.0f)
		{
			Destroy();
			return;
		}
	}

}