#pragma once

#include "../sprite/sprite.h"
#include "../property/property.h"
#include "../resources/resources.h"
#include "../weapon/damage-type.h"

namespace gameapp
{
	class Planet;

	struct UnitBlueprint
	{
		//------
		std::string m_typeName;

		// --------- static
		std::size_t m_buildTime;
		std::size_t m_fogOfWarExploreRadius; // na ile uk�ad�w widz� dooko�a siebie
		std::unordered_map<ResourceType, float> m_cost;

		// ------------- dynamic
		float m_initialHp;
		std::unordered_map<DamageType, float> m_initialResists;
	};


	class IUnit : public virtual RectangleSprite, public IProperty
	{
	public:
		IUnit(Planet* position, Player* owner, UnitBlueprint* blueprint);
		virtual ~IUnit() = default;
		void Destroy();

		void Update();

		virtual void PerformPrePhase() = 0;
		virtual void PerformPhase() = 0;
		virtual void PerformPostPhase() = 0;

		Planet* GetOccupiedPlanet() const;
		float GetHP() const;
		bool IsReady()const;

		const std::string& GetTypeName() const;

		void AbsorbDamage(const DamageType type, const float damage);
	protected:
		void SetOccupiedPlanet(Planet * planet);

	private:
		UnitBlueprint* m_unitBlueprint;
		Planet* m_occupiedPlanet;
		
		std::size_t m_buildPhase;
		bool m_built;

		float m_hp;
		std::unordered_map<DamageType, float> m_resists;
	};
}