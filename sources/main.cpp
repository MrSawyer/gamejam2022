#include "examples/sfml-renderer/sfml-renderer.h"
#include "game-app/game.h"

int main() {
	std::unique_ptr<hospite::Framework> framework = std::make_unique<hospite::Framework>();

	framework->RegisterApp(std::make_unique<gameapp::GameApp>());
	framework->RegisterRenderer(std::make_unique<SFMLRenderer>());

	if (!framework->Init())
		return -1;

	framework->Loop();

  return 0;
}