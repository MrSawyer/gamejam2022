#pragma once

#include "../macros/streamize.h"

#include <filesystem>
#include <fstream>
#include <sstream>

// uncomment to disable assert()
// #define NDEBUG

#define LOG_ENABLE 1

namespace hospite {

#if LOG_ENABLE

	const std::string LOG_FILE_PATH = "log";
	const std::string LOG_FILE_EXTENSION = "txt";
	const std::string PROJECT_NAME = "hospite";
	const std::string DIRECTORY_SEPARATOR = "\\";

	const unsigned int HIGHTLIGHT_COLOR = 10;
	const unsigned int INF_COLOR = 8;
	const unsigned int WAR_COLOR = 14;
	const unsigned int ERR_COLOR = 12;

#if NDEBUG
	const bool ASSERTION = false;
#else
	const bool ASSERTION = true;
#endif // _NDEBUG

	class Logger
	{
	public:
		Logger(Logger& other) = delete;
		Logger(const Logger&) = delete;
		Logger(Logger&&) = delete;
		Logger& operator=(const Logger&) = delete;
		Logger& operator=(Logger&&) = delete;

		~Logger();

		static Logger& get();

		void writeLogToFile(const std::stringstream& msg);

	private:
		Logger();

		std::fstream file;
	};

	void printLog(const std::stringstream& msg, unsigned int color);

#define GETINFO(...) std::string{" "} << EXPAND(STREAMIZE(__VA_ARGS__)) << ""


#define MAKE_LOG(lv, msg, color)                                                                                            \
	do {                                                                                                                    \
		std::stringstream log;                                                                                              \
		std::string		  relative_path(__FILE__);                                                                          \
		if (relative_path.find(hospite::PROJECT_NAME) != std::string::npos)                                              \
			relative_path = relative_path.substr(relative_path.find(hospite::PROJECT_NAME));                             \
		log << lv << "[" << relative_path << "][" << __func__ << "()](" << std::to_string(__LINE__) << ") " << msg << "\n"; \
		hospite::printLog(log, color);                                                                                   \
		hospite::Logger::get().writeLogToFile(log);                                                                      \
	} while (0);

#define ASSERT_MSG(condition, msg)                                        \
	do {                                                                  \
		if (condition)                                                    \
			break;                                                        \
		MAKE_LOG("ASSERT", "<" << #condition << " : False> " << msg, 12); \
		if (hospite::ASSERTION) {                                      \
			hospite::Logger::get().~Logger();                          \
			std::abort();                                                 \
		}                                                                 \
	} while (0);

#define ASSERT(condition) ASSERT_MSG(condition, "")

#define LOG_ERR(msg) MAKE_LOG("ERR", msg, hospite::ERR_COLOR);

#define LOG_WAR(msg) MAKE_LOG("WAR", msg, hospite::WAR_COLOR);

#define LOG_INF(msg) MAKE_LOG("INF", msg, hospite::INF_COLOR);

#define HIGHTLIGHT(msg) MAKE_LOG("INF", msg, hospite::HIGHTLIGHT_COLOR);

#else
#define MAKE_LOG(...)
#define ASSERT_MSG(...)
#define ASSERT(...)
#define LOG_ERR(...)
#define LOG_WAR(...)
#define LOG_INF(...)
#define HIGHTLIGHT(...)
#endif
} // namespace hospite