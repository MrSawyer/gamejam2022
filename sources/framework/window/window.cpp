#include <windowsx.h>
#include "window.h"
#include <memory>
#include "../logger/logger.h"

namespace hospite
{
	bool Window::st_classRegistered = false;
	const wchar_t* Window::st_className = L"HospiteWindowClass";

	Window::Window(const Resolution& resolution)
		: m_hwnd(NULL)
	{
		RegisterWndClass();
		CreateWnd(resolution);
		LOG_INF("Window created");
	}

	Window::~Window()
	{
		DestroyWnd();
		LOG_INF("Window destroyed");
	}

	void Window::SetListener(std::unique_ptr<FrameworkWindowListener> listener)
	{
		m_listener = std::move(listener);
	}

	LRESULT CALLBACK Window::StaticWndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		if (uMsg == WM_CREATE)
		{
			LPCREATESTRUCT data = (LPCREATESTRUCT)lParam;
			::SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG_PTR)data->lpCreateParams);
			auto* window = (Window*)data->lpCreateParams;
			window->m_hwnd = hWnd;
		}

		// Process messages by window message function
		auto window = (Window*) ::GetWindowLongPtr(hWnd, GWLP_USERDATA);
		if (window)
		{
			return window->WndProc(uMsg, wParam, lParam);
		}
		else
		{
			return static_cast<LRESULT>(DefWindowProc(hWnd, uMsg, wParam, lParam));
		}
	}

	LRESULT CALLBACK Window::WndProc(UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg)
		{
		// Window is closing
		case WM_CLOSE:
		{
			if (m_listener)
				m_listener->OnClose();

			PostQuitMessage(0);
			break;
		}

		// Key pressed
		case WM_KEYDOWN:
		{
			if (m_listener)
				m_listener->OnKeyPress((int)wParam);

			return 0;
		}
		// Mouse wheel moved
		case WM_MOUSEWHEEL:
		{
			if (m_listener)
				m_listener->OnMouseWheel((int)GET_WHEEL_DELTA_WPARAM(wParam) / WHEEL_DELTA);

			return 0;
		}
		// Mouse moved
		case WM_MOUSEMOVE:
		{
			if (m_listener)
				m_listener->OnMouseMoved((int)GET_X_LPARAM(lParam), (int)GET_Y_LPARAM(lParam));

			return 0;
		}
		//L Mouse click
		case WM_LBUTTONDOWN:
		{
			if (m_listener)
				m_listener->OnMouseButtonClicked(1);
			return 0;
		}

		//M Mouse click
		case WM_MBUTTONDOWN:
		{
			if (m_listener)
				m_listener->OnMouseButtonClicked(2);
			return 0;
		}
		//R Mouse click
		case WM_RBUTTONDOWN:
		{
			if (m_listener)
				m_listener->OnMouseButtonClicked(3);
			return 0;
		}
		}

		return static_cast<LRESULT>(DefWindowProc(m_hwnd, uMsg, wParam, lParam));
	}

	void Window::CreateWnd(const Resolution& resolution)
	{
		DWORD dwStyle = WS_POPUP | WS_VISIBLE;
		DWORD dxExStyle = 0;

		m_windowRect;
		m_windowRect.left = 50;
		m_windowRect.top = 50;
		m_windowRect.right = m_windowRect.left + (int)resolution.WIDTH;
		m_windowRect.bottom = m_windowRect.top + (int)resolution.HEIGHT;

		AdjustWindowRect(&m_windowRect, dwStyle, FALSE);

		CreateWindowW(
			st_className,
			L"Hospite",
			dwStyle,
			m_windowRect.left, m_windowRect.top,
			m_windowRect.right - m_windowRect.left,
			m_windowRect.bottom - m_windowRect.top,
			NULL, NULL, GetModuleHandle(NULL), this);

		ASSERT(m_hwnd != NULL);
	}

	HWND Window::CreateViewBuffer()
	{
		DWORD Style = WS_CHILD | WS_VISIBLE | WS_CLIPSIBLINGS;
		return CreateWindowW(L"STATIC", NULL, Style,
			m_windowRect.left, m_windowRect.top,
			m_windowRect.right - m_windowRect.left,
			m_windowRect.bottom - m_windowRect.top,
			m_hwnd, NULL, GetModuleHandle(NULL), this);
	}

	void Window::DestroyWnd()
	{
		if (m_hwnd != NULL)
		{
			DestroyWindow(m_hwnd);
			m_hwnd = nullptr;
		}
	}

	void Window::RegisterWndClass()
	{
		if (!st_classRegistered)
		{
			WNDCLASSEXW wcex;

			ZeroMemory(&wcex, sizeof(wcex));
			wcex.cbSize = sizeof(WNDCLASSEX);
			wcex.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
			wcex.lpfnWndProc = &StaticWndProc;
			wcex.cbClsExtra = 0;
			wcex.cbWndExtra = 0;
			wcex.hInstance = GetModuleHandle(NULL);
			wcex.hCursor = LoadCursor(NULL, IDC_ARROW);
			wcex.hbrBackground = CreateSolidBrush(RGB(0, 0, 0));
			wcex.lpszMenuName = NULL;
			wcex.lpszClassName = st_className;

			RegisterClassExW(&wcex);

			st_classRegistered = true;
		}
	}
}