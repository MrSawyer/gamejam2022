#pragma once
#include <Windows.h>
#include <memory>
#include "../framework-listener/framework-listener.h"
#include "resolution.h"

namespace hospite
{
	class Window
	{
	public:
		Window(const Resolution & resolution);
		~Window();

		inline HWND GetHandle() const { return m_hwnd; }

		void SetListener(std::unique_ptr<FrameworkWindowListener> listener);

		HWND CreateViewBuffer();

	private:
		void RegisterWndClass();
		void CreateWnd(const Resolution& resolution);
		void DestroyWnd();

		static LRESULT CALLBACK StaticWndProc(HWND, UINT, WPARAM, LPARAM);
		LRESULT CALLBACK WndProc(UINT, WPARAM, LPARAM);

		static bool			st_classRegistered;

		static const wchar_t* st_className;

		std::unique_ptr<FrameworkWindowListener>		m_listener;
		HWND					m_hwnd;
		RECT m_windowRect;
	};
}