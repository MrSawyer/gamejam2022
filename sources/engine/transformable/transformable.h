#pragma once

#include <glm/glm.hpp>
#include <functional>

namespace hospite
{
	class Transformable 
	{
	public:
		Transformable();
		explicit Transformable(glm::vec3 position);
		explicit Transformable(float pos_x, float pos_y, float pos_z);

		Transformable(const Transformable&);
		Transformable(Transformable&&)noexcept;
		Transformable& operator=(const Transformable&);
		Transformable& operator=(Transformable&&)noexcept;

		Transformable operator + (const Transformable&);

		virtual ~Transformable() = default;

		void SetPosition(glm::vec3 position);
		void SetPosition(float x, float y, float z);
		void SetPositionX(float x);
		void SetPositionY(float y);
		void SetPositionZ(float z);

		void SetRotation(glm::vec3 rotation);
		void SetRotation(float x, float y, float z);
		void SetRotationX(float x);
		void SetRotationY(float y);
		void SetRotationZ(float z);

		void SetScale(glm::vec3 scale);
		void SetScale(float x, float y, float z);
		void SetScaleX(float x);
		void SetScaleY(float y);
		void SetScaleZ(float z);

		glm::vec3 GetPosition() const;
		glm::vec3 GetRotation() const;
		glm::vec3 GetScale() const;

		float GetPositionX() const;
		float GetPositionY() const;
		float GetPositionZ() const;

		float GetRotationX() const;
		float GetRotationY() const;
		float GetRotationZ() const;

		float GetScaleX() const;
		float GetScaleY() const;
		float GetScaleZ() const;

		glm::mat4 GetMatrix();


		bool RecentlyUpdated()const;
		bool IsScheduledToUpdate() const;
		void SetUpdateCallback(std::function<void()> callback);

	private:
		void Update();

		glm::mat4 m_matrix;
		glm::vec3 m_position, m_rotation, m_scale;
		bool m_toUpdate;

		bool m_recentlyUpdated;
		std::function<void()> m_updateCallback;
	};
}